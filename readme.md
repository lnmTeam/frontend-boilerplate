# Frontend Boilerplate LNM

> Estructura frontend para los proyectos LNM.

La siguiente estructura es una referencia para los proyectos FrontEnd.
Luego de descargado el código fuente realizar las adaptaciones o cambios que sean necesarios para cada proyecto.

## Características

* [HTML5 Boilerplate](http://html5boilerplate.com/) front-end template.
* [Gruntjs](http://gruntjs.org/) for Server and Build tasks.
* [Bower](http://bower.io/) for dependencies managment.
* [Sass](http://http://sass-lang.com/) pre-processor (With [libsass](https://github.com/sass/libsass) compiler)
* [Bootstrap 4](http://getbootstrap.com/) CSS Framework v4 (or any you want)
* [jQuery](http://jquery.com/) JS Framework.
* [Modernizr](http://modernizr.com/) JS library.
* [PostCSS](https://github.com/postcss/postcss) and [Autoprefixer](https://github.com/postcss/autoprefixer)
* [Livereload](https://github.com/gruntjs/grunt-contrib-connect) and [watch files changes](https://github.com/gruntjs/grunt-contrib-watch)
* [Compress CSS files](https://github.com/gruntjs/grunt-contrib-cssmin)
* [Minify files with UglifyJS](https://github.com/gruntjs/grunt-contrib-uglify)
* [Just In Time plugin loader for Grunt tasks](https://github.com/shootaroo/jit-grunt)

## Requerimientos
Es necesario instalar previamente los siguientes paquetes:

* [Nodejs](http://nodejs.org/) >= 4
* [Gruntjs](http://gruntjs.com/) >= 0.4.0 - `npm install -g grunt-cli`
* [Bower](http://bower.io/) >= 1.3.10 - `npm install -g bower`

## Uso

### Instalación

#### 1) Crear directorio del proyecto

Crear un directorio para el nuevo proyecto:

```sh
mkdir nuevo-proyecto
```

Ingresar al directorio creado:

```sh
cd mkdir nuevo-proyecto
```

#### 2) Obtener la estructura frontend

La siguientes lineas de comandos obtienen únicamente los archivos de la estructura frontned ([frontend-boilerplate](https://bitbucket.org/lnmTeam/frontend-boilerplate))
En este caso los archivos se obtienen desde la versión `latest` (`git checkout latest`)
Esto es para prevenir conflictos por actualizaciones en el repositorio de [frontend-boilerplate](https://bitbucket.org/lnmTeam/frontend-boilerplate) y mantener así estabilidad del código.
Cuando se actualize el repositorio de la estructura frontend a una nueva version, simplemente cambiar el tag de la versión en la linea: `git checkout LA_VERSION`

Ejecutar la siguiente linea de comandos:

```sh
git init &&\
git remote add origin https://bitbucket.org/lnmTeam/frontend-boilerplate.git &&\
git fetch &&\
git checkout latest &&\
rm -rf .git
```

#### 3) Instalar dependencias

Finalmente, instalar los paquetes locales necesarios. `npm install` también instalará los paquetes `bower`.

```sh
$ npm install
```

### API

#### Serve
Inicia el servidor de pruebas para desarollo. El directorio de trabajo es `app/`

```sh
$ grunt serve
```

#### Build
Genera la carpeta `dist/` con los archivos para producción.

```sh
$ grunt build
```

#### Dist
Inicia el servidor de vizualización desde la carpeta `dist/`.

```sh
$ grunt dist
```

## Configuración

Para editar las rutas de los archivos y carpetas de desarollo editar la seccion `"frontr"`` en el archivo `package.json`.

```json
{
  "hostname": "localhost",
  "port": 7001,
  "app": "/app",
  "dist": "/dist",
  "assets": {
    "dirname": "/assets",
    "src": {
      "css": "",
      "js": "/js",
      "img": "/img",
      "fonts": "/fonts"
    },
    "style": "style"
  },
  "uncssIgnore": ".uncss-?\\S*"
}
```

## Contribución
Para contribuir a este repositorio con mejoras o correciones de errores haz un fork y luego un pull request.

## Desarrolladores
* [José Luis Quintana](mailto:jose.luis@lanaranjamedia.com)

© 2016 [La Naranja Media](http://lanaranjamedia.com/)

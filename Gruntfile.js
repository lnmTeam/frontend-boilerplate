module.exports = function (grunt) {
  require('time-grunt')(grunt)
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  })

  // Configuration
  var pkg = grunt.file.readJSON('package.json')
  var config = pkg.frontr

  config.app = '.' + config.app
  config.dist = '.' + config.dist

  config.assetsApp = config.app + config.assets.dirname
  config.assetsDistDir = config.dist + config.assets.dirname

  config.cssDir = config.assets.dirname + config.assets.src.css
  config.jsDir = config.assets.dirname + config.assets.src.js
  config.imgDir = config.assets.dirname + config.assets.src.img
  config.fontsDir = config.assets.dirname + config.assets.src.fonts

  grunt.initConfig({
    config: config,
    pkg: pkg,
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      sass: {
        files: [
          '<%= config.app %>/<%= config.assets.style %>.{scss,sass}',
          '<%= config.assetsApp %>/scss/**/*.{scss,sass}'
        ],
        tasks: ['sass', 'postcss']
      },
      js: {
        files: ['<%= config.app %><%= config.jsDir %>/{,*/}*.js']
      },
      test: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['mocha']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      }
    },
    browserSync: {
      options: {
        notify: false,
        background: true,
        watchOptions: {
          ignored: ''
        }
      },
      livereload: {
        options: {
          files: [
            '<%= config.app %>/{,*/}*.html',
            '<%= config.app %><%= config.imgDir %>/{,*/}*.{gif,ico,jpg,jpeg,png,svg,webp}',
            '.tmp/<%= config.assets.src.css %>/{,*/}*.css',
            '<%= config.app %><%= config.jsDir %>/{,*/}*.js'
          ],
          port: config.port,
          server: {
            baseDir: ['.tmp', config.app],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      },
      dist: {
        options: {
          port: config.port,
          background: false,
          server: '<%= config.dist %>'
        }
      },
      test: {
        options: {
          port: config.port + 2,
          host: config.hostname,
          files: [
            'test/{,*/}*.html',
            'test/spec/*.js'
          ],
          server: {
            baseDir: ['./test'],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },
    sass: {
      options: {
        sourceMap: true,
        sourceMapEmbed: true,
        sourceMapContents: true,
        includePaths: ['.']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>',
          src: ['<%= config.assets.style %>.{scss,sass}'],
          dest: '.tmp/<%= config.assets.src.css %>',
          ext: '.css'
        }]
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: 'last 4 version'
          })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/<%= config.app %><%= config.assets.src.css %>/',
          src: '{,*/}*.css',
          dest: '.tmp/<%= config.app %><%= config.assets.src.css %>/'
        }]
      }
    },
    wiredep: {
      app: {
        src: ['<%= config.app %>/*.html'],
        sass: {
          src: ['<%= config.app %>/<%= config.assets.style %>.{scss,sass}'],
          ignorePath: /^(\.\.\/)+/
        }
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '{,*/}*.{html,ico,png,txt,xml,htaccess,appcache,mf}',
            '.<%= config.imgDir %>/**/*',
            '.<%= config.fontsDir %>/{,*/}*',
            '.<%= config.jsDir %>/vendor/{,*/}*',
            '.<%= config.jsDir %>/templates/{,*/}*'
          ]
        }]
      },
      tmp: {
        files: [{
          expand: true,
          dot: true,
          cwd: '.tmp',
          dest: '<%= config.dist %>',
          src: [
            './{,*/}*.css',
            '.<%= config.assets.dirname %><%= config.assets.src.js %>/{,*/}*.js'
          ]
        }]
      },
      test: {
        files: [{
          expand: true,
          dot: true,
          cwd: './test',
          dest: '.tmp/test',
          src: [
            '{,*/}*.html'
          ]
        }]
      }
    },
    uglify: {
      options: {
        mangle: false
      }
    },
    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dist %>/',
          src: [
            '*.css',
            '!*.min.css'
          ],
          dest: '<%= config.dist %>/',
          ext: '.css'
        }]
      }
    },
    useminPrepare: {
      options: {
        dest: '<%= config.dist %>'
      },
      html: '<%= config.app %>/index.html'
    },
    usemin: {
      options: {
        assetsDirs: [
          '<%= config.dist %>',
          '<%= config.assetsDistDir %>'
        ]
      },
      html: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/<%= config.cssDir %>/{,*/}*.css']
    },
    mocha: {
      all: {
        options: {
          run: true,
          urls: ['http://<%= config.hostname %>:<%= config.port + 2 %>/index.html'],
          reporter: 'Spec'
        }
      }
    }
  })

  // Serve Tasks
  grunt.registerTask('serve', 'Start the development server and preview your app.' +
  ' --remote-access to allow remote access',
    function () {
      if (grunt.option('remote-access')) {
        grunt.config.set('connect.options.hostname', '0.0.0.0')
      }

      grunt.task.run([
        'clean:server',
        'sass',
        'postcss:dist',
        'browserSync:livereload',
        'watch'
      ])
    })

  // Dist Tasks
  grunt.registerTask('dist', 'Start the dist server and preview your app.' +
  '--remote-access to allow remote access',
    function () {
      if (grunt.option('remote-access')) {
        grunt.config.set('connect.options.hostname', '0.0.0.0')
      }

      grunt.task.run([
        'browserSync:dist',
        'watch'
      ])
    })

  // Build Tasks
  grunt.registerTask('build', function () {
    if (grunt.option('css')) {
      grunt.task.run([
        'clean:dist',
        'sass:dist',
        'copy:tmp',
        'postcss:dist',
        'cssmin:dist'
      ])

      return
    }

    if (grunt.option('js')) {
      grunt.task.run([
        'clean:dist',
        'copy:tmp',
        'copy:dist',
        'useminPrepare',
        'concat',
        'uglify',
        'usemin'
      ])

      return
    }

    grunt.task.run([
      'clean:dist',
      'sass:dist',
      'copy:tmp',
      'copy:dist',
      'useminPrepare',
      'concat',
      'uglify',
      'postcss:dist',
      'cssmin:dist',
      'usemin'
    ])
  })

  // Test Tasks
  grunt.registerTask('test', function () {
    if (!grunt.option('watch')) {
      grunt.config.set('browserSync.test.options.open', false)
      grunt.config.set('browserSync.test.options.logLevel', 'silent')
      grunt.config.set('browserSync.test.options.files', [])
    }

    grunt.task.run([
      'browserSync:test',
      'mocha'
    ])

    if (grunt.option('watch')) {
      grunt.task.run([
        'watch:test'
      ])
    }
  })
}
